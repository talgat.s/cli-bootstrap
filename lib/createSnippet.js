const fs = require('fs');
const util = require('util');
const {prompt} = require('enquirer');
const Handlebars = require('handlebars');
const chalk = require('chalk');
const createFile = require('./createFile');
const readFile = require('./readFile');
const registerHelper = require('./registerHelpers');

registerHelper(Handlebars);
const access = util.promisify(fs.access);

async function writeIntoFile(resultPath) {
	try {
		await access(resultPath);
	} catch (e) {
		return true;
	}

	const {overwrite} = await prompt([
		{
			type: 'confirm',
			name: 'overwrite',
			message: `${chalk.magenta(resultPath)} file already exists. Overwrite?`
		}
	]);

	return overwrite;
}

module.exports = async (resultPath, tmlPath, values, middleware) => {
	const flag = await writeIntoFile(resultPath);

	if (flag) {
		const template = Handlebars.compile(await readFile(tmlPath))(values);
		await createFile(
			resultPath,
			typeof middleware === 'function' ? middleware(template) : template
		);
	}
};
